import {DayData} from "../shared/typings";
import {dayNames, monthNames} from "../shared/constants";
import {averageWithRounding, to2DigitsAfterComa} from "../shared/utils";
import {groupBy} from "lodash";

export const process = (tickerData: DayData[], previousDayGrowthFrom?: number, previousDayGrowthTo?: number, month?: number) => {
    for (let i = 0; i < tickerData.length; i++) {
        if (i !== 0) {
            tickerData[i].previous = tickerData[i - 1]
        }
        if (i !== tickerData.length - 1) {
            tickerData[i].next = tickerData[i + 1]
        }
    }
    if (previousDayGrowthFrom !== null && previousDayGrowthFrom !== undefined) {
        tickerData = tickerData.filter(it =>
            it.previous && it.previous.growthInDayNormalizedInPercent &&
            previousDayGrowthFrom <= it.previous.growthInDayNormalizedInPercent)
    }
    if (previousDayGrowthTo !== null && previousDayGrowthTo !== undefined) {
        tickerData = tickerData.filter(it =>
            it.previous && it.previous.growthInDayNormalizedInPercent &&
            previousDayGrowthTo >= it.previous.growthInDayNormalizedInPercent)
    }
    if (month) {
        tickerData = tickerData.filter(it => it.month === month)
    }
    return tickerData;
};

export const calcDaysAverage = (data: DayData[]) => {
    const groupedByDayOfWeek = groupBy(data, 'dayOfWeek');
    return Object.keys(groupedByDayOfWeek).map(dayOfWeek => ({
        dayOfWeek: dayNames[parseInt(dayOfWeek) - 1],
        percent: averageWithRounding(groupedByDayOfWeek[dayOfWeek].map(it => it['growthInDayNormalizedInPercent']))
    }));
};

export const calcMonthDaysAverage = (data: DayData[]) => {
    const groupedByDayOfWeek = groupBy(data, 'day');
    return Object.keys(groupedByDayOfWeek).map(dayOfMonth => ({
        dayOfMonth,
        percent: averageWithRounding(groupedByDayOfWeek[dayOfMonth].map(it => it['growthInDayNormalizedInPercent']))
    }));
};

export const generateDateRange = (index: number) => dayNames[index].substr(0, 3) + '-' + dayNames[index + 1].substr(0, 3);

export const calcDaysAfterMarketAverage = (data: DayData[]) => {
    const groupedByDayOfWeek = groupBy(data, 'dayOfWeek');
    return Object.keys(groupedByDayOfWeek).map(dayOfWeek => {
        const percent = averageWithRounding(groupedByDayOfWeek[dayOfWeek]
            .filter(it => it.next)
            .map(it => ((it.next.open / it.close) - 1) * 100));
        return ({
            dayOfWeek: generateDateRange(parseInt(dayOfWeek) - 1),
            percent: percent
        });
    });
};

export const calcMonthsAverage = (growthInMonthWithinYears: { month: string, growthPercent: number, year: string }[]) => {
    const groupedByMonth = groupBy(growthInMonthWithinYears, 'month');
    return Object.keys(groupedByMonth).map(month => ({
        month,
        percent: averageWithRounding(groupedByMonth[month].map(it => it.growthPercent))
    }))
};

export const calcYearsAverage = (data: DayData[]) => {
    const groupedByYear = groupBy(data, 'year');
    return Object.keys(groupedByYear).map(year => {
        const groupedByYearElement = groupedByYear[year];
        return {
            year: year,
            percent: to2DigitsAfterComa(((groupedByYearElement[groupedByYearElement.length - 1].open / groupedByYearElement[0].close) - 1) * 100)
        };
    });
};

export const getFlatGrowthPerMonthWithYear = (data: DayData[]) => {
    const growthInMonthWithinYears: { month: string, growthPercent: number, year: string }[] = [];
    const groupedByMonths = groupBy(data, 'month');
    Object.keys(groupedByMonths).forEach(month => {
        const groupedByAndMonths = groupBy(groupedByMonths[month], 'year');
        Object.keys(groupedByAndMonths).forEach((year) => {
            const yearMonth = groupedByAndMonths[year];
            growthInMonthWithinYears.push({
                month: monthNames[parseInt(month) - 1],
                year,
                growthPercent: to2DigitsAfterComa(((yearMonth[yearMonth.length - 1].open / yearMonth[0].close) - 1) * 100)
            })
        })
    });
    return growthInMonthWithinYears;
};