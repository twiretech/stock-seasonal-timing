import styles from "../styles/Tables.module.css";
import {
    Bar,
    BarChart,
    CartesianGrid,
    Legend,
    ReferenceLine,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";
import {averageWithRounding} from "../shared/utils";

const Charts = ({
                    tickerDayData,
                    tickerAfterMarketDayData,
                    tickerMonthData,
                    tickerYearData,
                    tickerMonthsYearsData,
                    tickerMonthDaysData
                }: {
    tickerDayData?: {percent: number, dayOfWeek: string}[],
    tickerAfterMarketDayData?: any[],
    tickerMonthData?: any[],
    tickerYearData?: any[],
    tickerMonthsYearsData?: any[],
    tickerMonthDaysData?: any[]
}) => {
    const minHeight = "300px";
    return <div className={styles.tables}>
        {tickerDayData && <div className={styles.table}>
            <h3 className={styles.tableHeading}>Average growth per day trading hours in percent</h3>
            <h4 className={styles.tableSubHeading}>((close - open) / open)</h4>
            <p className={styles.center}>Average growth during trading hours is {averageWithRounding(tickerDayData.map(it => it.percent))}%</p>
            <ResponsiveContainer className={styles.tableChartContainer} width="100%" height="100%"
                                 minHeight={minHeight}>
                <BarChart
                    data={tickerDayData}
                >
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="dayOfWeek"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    <ReferenceLine y={0} stroke="#000"/>
                    <Bar dataKey="percent" fill="#8884d8"/>
                </BarChart>
            </ResponsiveContainer>
        </div>}
        {tickerAfterMarketDayData && <div className={styles.table}>
            <h3 className={styles.tableHeading}>Average growth per day outside trading hours in percent</h3>
            <h4 className={styles.tableSubHeading}>((next day open - previous day close) / previous day close)</h4>
            <p className={styles.center}>Average growth outside trading hours is {averageWithRounding(tickerAfterMarketDayData.map(it => it.percent))}%</p>
            <ResponsiveContainer className={styles.tableChartContainer} width="100%" height="100%"
                                 minHeight={minHeight}>
                <BarChart
                    data={tickerAfterMarketDayData}
                >
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="dayOfWeek"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    <ReferenceLine y={0} stroke="#000"/>
                    <Bar dataKey="percent" fill="#8884d8"/>
                </BarChart>
            </ResponsiveContainer>
        </div>}
        {tickerMonthDaysData && <div className={styles.table}>
            <h3 className={styles.tableHeading}>Average growth per day in a month in percent</h3>
            <h4 className={styles.tableSubHeading}>((close - open) / open)</h4>
            <p className={styles.center}>Average growth during 24h is {averageWithRounding(tickerMonthDaysData.map(it => it.percent))}%</p>
            <ResponsiveContainer className={styles.tableChartContainer} width="100%" height="100%"
                                 minHeight={minHeight}>
                <BarChart data={tickerMonthDaysData}>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="dayOfMonth"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    <ReferenceLine y={0} stroke="#000"/>
                    <Bar dataKey="percent" fill="#8884d8"/>
                </BarChart>
            </ResponsiveContainer>
        </div>}
        {tickerMonthData && <div className={styles.table}>
            <h3 className={styles.tableHeading}>Average growth per month in percent</h3>
            <h4 className={styles.tableSubHeading}>((close - open) / open)</h4>
            <p className={styles.center}>Average per month is {averageWithRounding(tickerMonthData.map(it => it.percent))}%</p>
            <ResponsiveContainer className={styles.tableChartContainer} width="100%" height="100%"
                                 minHeight={minHeight}>
                <BarChart
                    data={tickerMonthData}
                >
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="month"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    <ReferenceLine y={0} stroke="#000"/>
                    <Bar dataKey="percent" fill="#8884d8"/>
                </BarChart>
            </ResponsiveContainer>
        </div>}
        {tickerYearData && <div className={styles.table}>
            <h3 className={styles.tableHeading}>Average growth per year in percent</h3>
            <h4 className={styles.tableSubHeading}>((close - open) / open)</h4>
            <p className={styles.center}>Average yearly growth is {averageWithRounding(tickerYearData.map(it => it.percent))}%</p>
            <ResponsiveContainer className={styles.tableChartContainer} width="100%" height="100%"
                                 minHeight={minHeight}>
                <BarChart
                    data={tickerYearData}
                >
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="year"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    <ReferenceLine y={0} stroke="#000"/>
                    <Bar dataKey="percent" fill="#8884d8"/>
                </BarChart>
            </ResponsiveContainer>
        </div>}
        {tickerMonthsYearsData && <div className={styles.table}>
            <h3 className={styles.tableHeading}>Average growth by months in percent</h3>
            <h4 className={styles.tableSubHeading}>((close - open) / open)</h4>
            <ResponsiveContainer className={styles.tableChartContainer} width="100%" height="100%"
                                 minHeight={minHeight}>
                <BarChart
                    data={tickerMonthsYearsData}
                >
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="month"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    <ReferenceLine y={0} stroke="#000"/>
                    <Bar dataKey="growthPercent"/>
                </BarChart>
            </ResponsiveContainer>
        </div>}
    </div>
}

export default Charts