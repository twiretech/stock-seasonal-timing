import {useEffect, useState} from "react";
import styles from "../styles/Tables.module.css";
import {useRouter} from "next/router";

const SearchBar = ({
                       year,
                       ticker,
                       previousDayGrowthFrom,
                       month,
                       previousDayGrowthTo,
                       numberOfSamples,
                       error,
                       setError,
                       setLoading,
                       setLoadingSplashScreen,
                       loading
                   }: {
    year: number,
    ticker: string,
    previousDayGrowthFrom?: number,
    month?: number,
    previousDayGrowthTo?: number,
    numberOfSamples?: number,
    error: any,
    loading: boolean,
    setError: (a: any) => void,
    setLoading: (a: boolean) => void,
    setLoadingSplashScreen: (a: boolean) => void
}) => {
    const router = useRouter();
    const [tickerInput, setTickerInput] = useState(ticker);
    const [yearInput, setYearInput] = useState(year);
    const [monthInput, setMonthInput] = useState(month?.toString());
    const [showAdditionalFilters, setShowAdditionalFilters] = useState(!!((previousDayGrowthFrom ?? previousDayGrowthTo) ?? month));
    const [previousDayGrowthFromInput, setPreviousDayGrowthFromInput] = useState(previousDayGrowthFrom?.toString());
    const [previousDayGrowthToInput, setPreviousDayGrowthToInput] = useState(previousDayGrowthTo?.toString());

    useEffect(() => {
        setTickerInput(ticker)
    }, [ticker])

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (error) {
            setError(null);
            setLoadingSplashScreen(true);
        }
        if (tickerInput === ticker && yearInput === year &&
            parseFloat(previousDayGrowthFromInput!) === previousDayGrowthFrom &&
            parseFloat(previousDayGrowthToInput!) === previousDayGrowthTo) {
            return;
        }

        const query: any = {startYear: yearInput};
        if (previousDayGrowthFromInput) {
            query.previousDayGrowthFrom = previousDayGrowthFromInput;
        }
        if (previousDayGrowthToInput) {
            query.previousDayGrowthTo = previousDayGrowthToInput;
        }
        if (monthInput) {
            query.month = monthInput;
        }

        setLoading(true);
        router.push({
            pathname: `/ticker/${tickerInput}`,
            query,
        })
    }

    return <form className="pure-form" onSubmit={(e) => handleSubmit(e)}>
        <fieldset className={styles.fieldset}>
            <div>
                <label htmlFor="starting-year">Starting year: </label>
                <input id="starting-year"
                       className="desktop-right-gutter"
                       required={true}
                       type="number"
                       step="1"
                       value={yearInput}
                       onChange={e => setYearInput(parseInt(e.target.value))}/>
                <label htmlFor="ticker">Ticker: </label>
                <input id="ticker"
                       className="desktop-right-gutter"
                       required={true}
                       value={tickerInput}
                       onChange={e => setTickerInput(e.target.value)}/>
                {showAdditionalFilters && <>
                    <br/>
                    <br className="hide-mobile"/>
                    <span>Previous day growth between: </span>
                    <input className="desktop-right-gutter mobile-width-100"
                           placeholder="%"
                           type="number"
                           min="-1000"
                           max={1000}
                           step="0.5"
                           value={previousDayGrowthFromInput}
                           onChange={e => setPreviousDayGrowthFromInput(e.target.value)}/>
                    <span className="desktop-right-gutter">and</span>
                    <input className="desktop-right-gutter mobile-width-100"
                           placeholder="%"
                           type="number"
                           min="-1000"
                           max={1000}
                           step="0.5"
                           value={previousDayGrowthToInput}
                           onChange={e => setPreviousDayGrowthToInput(e.target.value)}/>
                    <br/>
                    <br className="hide-mobile"/>
                    <label htmlFor="month">Month: </label>
                    <input id="month"
                           type="number"
                           className="desktop-right-gutter mobile-width-100"
                           min={1}
                           max={12}
                           value={monthInput}
                           onChange={e => setMonthInput(e.target.value)}/>
                    <br/>
                    <br className="hide-mobile"/>
                    <label htmlFor="samples">Number of days/samples: </label>
                    <input id="samples"
                           type="number"
                           className="desktop-right-gutter"
                           readOnly
                           value={numberOfSamples}/>
                </>}
            </div>
            <div>
                            <span className="link hide-desktop" style={{textAlign: "center"}}
                                  onClick={() => setShowAdditionalFilters(!showAdditionalFilters)}>More filters</span>
                {!loading && <button type="submit"
                                     className="mobile-expandable-button pure-button pure-button-primary">Search</button>}
                {loading && <button type="submit" disabled={true}
                                    className="mobile-expandable-button pure-button pure-button-primary pure-button-disabled">Loading...</button>}
                <span className="desktop-right-gutter"/>
                <span className="link hide-mobile" style={{textAlign: "center"}}
                      onClick={() => setShowAdditionalFilters(!showAdditionalFilters)}>More filters</span>
            </div>
        </fieldset>
    </form>;
}

export default SearchBar