import {useEffect, useState} from "react";
import styles from "../styles/Tables.module.css";
import {DayData} from "../shared/typings";
import Charts from "./Charts";
import Links from "./Links";
import SearchBar from "./SearchBar";
import {
    calcDaysAfterMarketAverage,
    calcDaysAverage, calcMonthDaysAverage,
    calcMonthsAverage, calcYearsAverage,
    getFlatGrowthPerMonthWithYear, process
} from "./calculate";

const Tables = ({
                    ticker,
                    year,
                    month,
                    previousDayGrowthFrom,
                    previousDayGrowthTo
                }: { ticker: string, year: number, month?: number, previousDayGrowthFrom?: number, previousDayGrowthTo?: number }) => {
    const [tickerDayData, setTickerDayData] = useState<any>();
    const [tickerAfterMarketDayData, setTickerAfterMarketDayData] = useState<any[]>();
    const [tickerMonthData, setTickerMonthData] = useState<any[]>();
    const [tickerYearData, setTickerYearData] = useState<any[]>();
    const [tickerMonthsYearsData, setTickerMonthsYearsData] = useState<any[]>();
    const [tickerMonthDaysData, setTickerMonthDaysData] = useState<any[]>();

    const [firstLoad, setLoadingSplashScreen] = useState(true);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState<any>();

    const [numberOfSamples, setNumberOfSamples] = useState<number>();

    useEffect(() => {
        setLoading(true);
        fetch(`/api/ticker?startYear=${year}&ticker=${ticker}`)
            .then(data => !data.ok ? Promise.reject(data.text()) : data.json())
            .catch(_ => {
                setLoading(false);
                setError(<span>Error loading ticker <span
                    className={styles.error__ticker}>${ticker.toUpperCase()}</span>, try some another ticker</span>);
            })
            .then((tickerData: DayData[]) => {
                tickerData = process(tickerData, previousDayGrowthFrom, previousDayGrowthTo, month);
                setNumberOfSamples(tickerData.length)
                try {
                    const daysAverage = calcDaysAverage(tickerData);
                    const flatGrowthPerMonthWithYear = getFlatGrowthPerMonthWithYear(tickerData);
                    const monthsAverage = calcMonthsAverage(flatGrowthPerMonthWithYear);
                    const yearsAverage = calcYearsAverage(tickerData);
                    const afterDays = calcDaysAfterMarketAverage(tickerData);
                    const monthDaysAverage = calcMonthDaysAverage(tickerData);

                    setTickerDayData(daysAverage)
                    setTickerAfterMarketDayData(afterDays)
                    setTickerMonthDaysData(monthDaysAverage)
                    setTickerMonthData(monthsAverage);
                    setTickerYearData(yearsAverage);
                    setTickerMonthsYearsData(flatGrowthPerMonthWithYear);

                    setLoadingSplashScreen(false);
                    setError(null);
                } catch (e) {
                    setError(<span>{e as string}</span>)
                } finally {
                    setLoading(false);
                }
            })
    }, [ticker, year, previousDayGrowthFrom, previousDayGrowthTo, month])

    return (
        <>
            <div className={styles.filter}>
                <SearchBar
                    year={year}
                    previousDayGrowthFrom={previousDayGrowthFrom}
                    previousDayGrowthTo={previousDayGrowthTo}
                    month={month}
                    ticker={ticker}
                    numberOfSamples={numberOfSamples}
                    loading={loading}
                    error={error}
                    setLoading={setLoading}
                    setLoadingSplashScreen={setLoadingSplashScreen}
                    setError={setError}/>
                <Links/>
            </div>
            {error && <div className={styles.skeleton}><span className={styles.error}>{error}</span></div>}
            {firstLoad && <div className={styles.skeleton}>Loading...</div>}
            {!error && <Charts
                tickerDayData={tickerDayData}
                tickerAfterMarketDayData={tickerAfterMarketDayData}
                tickerMonthData={tickerMonthData}
                tickerYearData={tickerYearData}
                tickerMonthsYearsData={tickerMonthsYearsData}
                tickerMonthDaysData={tickerMonthDaysData}
            />}
        </>
    )
}

export default Tables