import styles from "../styles/Tables.module.css";
import Link from 'next/link';

const Links = () => <span className={styles.tickers}>
                <Link href="/ticker/QQQ">
                    <a className="link">QQQ</a>
                </Link>
    &nbsp;&nbsp;
    <Link href="/ticker/SPY">
                    <a className="link">SPY</a>
                </Link>
    &nbsp;&nbsp;
    <Link href="/ticker/AAPL">
                    <a className="link">AAPL</a>
                </Link>
    &nbsp;&nbsp;
    <Link href="/ticker/MSFT">
                    <a className="link">MSFT</a>
                </Link>
    &nbsp;&nbsp;
    <Link href="/ticker/FB">
                    <a className="link">FB</a>
                </Link>
    &nbsp;&nbsp;
    <Link href="/ticker/AMZN">
                    <a className="link">AMZN</a>
                </Link>
    &nbsp;&nbsp;
    <Link href="/ticker/AMD">
                    <a className="link">AMD</a>
                </Link>
    &nbsp;&nbsp;
    <Link href="/ticker/TSLA">
                    <a className="link">TSLA</a>
                </Link>
    &nbsp;&nbsp;
    <Link href="/ticker/NVDA">
                    <a className="link">NVDA</a>
                </Link>
            </span>

export default Links