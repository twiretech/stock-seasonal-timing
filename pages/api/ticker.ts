// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type {NextApiRequest, NextApiResponse} from 'next'
import fetch from 'node-fetch';
import {memoize} from 'lodash';
import {isPositiveNumberString, week} from "../../shared/utils";

const fetchByTicker = (ticker: string) => fetch(`https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${ticker}&apikey=4AWGY6SDNS53SKZG&outputsize=full`)
      .then(response => response.json());


const parseTickerData = (data: any, startYear: number) => {
  const exracted = Object
      .entries(data['Time Series (Daily)'])
      .filter(it => parseInt(it[0].substring(0, 4)) >= startYear).reverse()
  const parsed = [];
  for (let i = 0; i < exracted.length; i++){
    const [key, value]: [string, any] = exracted[i];
    const dates = key.replace('-', '').replace('-', '');
    const year = parseInt(dates.substring(0, 4));
    const month = parseInt(dates.substring(4, 6));
    const day = parseInt(dates.substring(6));
    const open = parseFloat(value['1. open']);
    const high = parseFloat(value['2. high']);
    const low = parseFloat(value["3. low"]);
    const close = parseFloat(value["4. close"]);
    const volume = parseFloat(value["5. volume"]);
    const dayOfWeek = new Date(key).getDay();
    const highNormalised = high / 300;
    const lowNormalised = low / 300;
    const growthInDay = close - open;
    const growthInDayNormalizedInPercent = growthInDay * 100 / open;
    const updated = {
      key,
      year,
      month,
      day,
      weekNr: week(year, month, day),
      open,
      high,
      highNormalised,
      low,
      lowNormalised,
      close,
      volume,
      dayOfWeek,
      growthInDay,
      growthInDayNormalizedInPercent
    };
    parsed.push(updated);
  }
  return parsed;
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<object>
) {
  if (!req.query?.ticker) {
    res.status(500).json({error: 'no ticker query param present'});
  }
  if (!req.query?.startYear) {
    res.status(500).json({error: 'no startYear query param present'});
  }
  if(!isPositiveNumberString(req.query.startYear as string)){
    res.status(500).json({error: 'startYear query param is incorrect'});
  }
  return fetchByTicker(req.query.ticker as string)
      .then((json: any) => res.json(parseTickerData(json, parseInt(req.query.startYear as string))))
      .catch(error => res.status(500).json(error));
}
