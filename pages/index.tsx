import type {InferGetServerSidePropsType, NextPage} from 'next'
import {GetServerSidePropsContext} from "next";

export async function getServerSideProps(context: GetServerSidePropsContext) {
    const {ticker, startYear} = context.query;
    if (!ticker || !startYear) {
        return {
            redirect: {
                destination: '/ticker/SPY?startYear=1990',
            },
        }
    }
    return {
        props: context.query // will be passed to the page component as props
    }
}


const Home: NextPage = () =>
    <div>Redirect...</div>


export default Home
