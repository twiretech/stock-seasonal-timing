import type {InferGetServerSidePropsType, NextPage} from 'next'
import styles from './../../styles/Home.module.css'
import Tables from "./../../components/Tables";
import {GetServerSidePropsContext} from "next";

export async function getServerSideProps(context: GetServerSidePropsContext) {
    const {ticker, startYear} = context.query;
    if (!ticker || !startYear) {
        return {
            redirect: {
                destination: `/ticker/${ticker || 'SPY'}?startYear=${startYear || 1990}`,
            },
        }
    }
    return {
        props: context.query // will be passed to the page component as props
    }
}


const Ticker: NextPage = ({
                              ticker,
                              startYear,
                              previousDayGrowthFrom,
                              previousDayGrowthTo,
                              month
                          }: any) => (
    <>
        <header className={styles.header}>
            <h1 style={{textAlign: "center"}}>{ticker} average performance</h1>
        </header>

        <main className={styles.main}>
            <Tables ticker={ticker}
                    year={startYear && parseInt(startYear)}
                    month={month && parseInt(month)}
                    previousDayGrowthFrom={previousDayGrowthFrom && parseFloat(previousDayGrowthFrom)}
                    previousDayGrowthTo={previousDayGrowthTo && parseFloat(previousDayGrowthTo)}/>
        </main>
    </>
)

export default Ticker
