import '../styles/globals.css'
import type { AppProps } from 'next/app'
import {useEffect} from "react";
import * as ga from '../shared/ga'
import { useRouter } from 'next/router';
import styles from "../styles/Home.module.css";
import Head from 'next/head'
import Image from 'next/image'

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter()

  //https://mariestarck.com/add-google-analytics-to-your-next-js-application-in-5-easy-steps/
  useEffect(() => {
    const handleRouteChange = (url: string) => {
      ga.pageview(url)
    }
    //When the component is mounted, subscribe to router changes
    //and log those page views
    router.events.on('routeChangeComplete', handleRouteChange)

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])


  return <div className={styles.container}>
    <Head>
      <title>Market backtesting tool</title>
      <meta name="description" content="Market backtesting tool"/>
      <link rel="icon" href="/favicon.ico"/>
      <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css"
            integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5"
            crossOrigin="anonymous"/>
      <link rel="preconnect" href="https://fonts.googleapis.com"/>
      <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin={'true'}/>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet"/>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-BHKLYW3KN5"/>
      <script
          dangerouslySetInnerHTML={{
            __html: `window.dataLayer = window.dataLayer || [];
                                  function gtag(){dataLayer.push(arguments);}
                                  gtag('js', new Date());
                                  gtag('config', 'G-BHKLYW3KN5');`,
          }}
      />
    </Head>

    <Component {...pageProps} />

    <footer className={styles.footer}>
      <a
          href="https://twire.ee"
          target="_blank"
          rel="noopener noreferrer"
          style={{display: "flex", alignItems: "center"}}
      >
        Powered by{'  '} &nbsp;
        <Image src={'/White logo - no background.svg'}
               width={100}
               height={40}
        />
      </a>
    </footer>
  </div>
}
export default MyApp
