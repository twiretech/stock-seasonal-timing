export const pageview = (url: string) => {
    (window as any).gtag('config', "G-BHKLYW3KN5", {
        page_path: url,
    })
}

// @ts-ignore
export const event = ({action, params}) => {
    (window as any).gtag('event', action, params)
}