export type DayData = {
    year: number,
    month: number,
    day: number,
    open: number,
    close: number,
    growthInDayNormalizedInPercent: number
    previous: DayData,
    next: DayData
};
