export function week(year:  number | string, month:  number, day:  number): number {
    function serial(days: number) {
        return 86400000 * days;
    }

    function dateserial(year: number, month: number , day: number) {
        return (new Date(year, month - 1, day).valueOf());
    }

    function weekday(date: any) {
        return (new Date(date)).getDay() + 1;
    }

    function yearserial(date: any) {
        return (new Date(date)).getFullYear();
    }

    //@ts-ignore
    const date = year instanceof Date ? year.valueOf() : typeof year === "string" ? new Date(year).valueOf() : dateserial(year, month, day),
        //@ts-ignore
        date2 = dateserial(yearserial(date - serial(weekday(date - serial(1))) + serial(4)), 1, 3);
    //@ts-ignore
    return ~~((date - date2 + serial(weekday(date2) + 5)) / serial(7));
}

export function isPositiveNumberString(value: string) {
    return /^\d+$/.test(value);
}

export function averageWithRounding(nums: number[]) {
    return to2DigitsAfterComa(nums.reduce((a, b) => (a + b)) / nums.length);
}

export function to2DigitsAfterComa(num: number){
    return Math.round(num * 100) / 100
}